import { NgModule } from '@angular/core';
import { MoviesComponent } from './movies.component';
import { MoviesRoutingModule } from './movies-routing.module';
import {AppModule} from "../../app.module";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    MoviesComponent,
  ],
  imports: [
    MoviesRoutingModule,
    AppModule,
    CommonModule,
  ],
  providers: []
})
export class MoviesModule { }
