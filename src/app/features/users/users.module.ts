import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import {AppModule} from "../../app.module";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    UsersComponent,
  ],
  imports: [
    UsersRoutingModule,
    AppModule,
    CommonModule,
  ],
  providers: []
})
export class UsersModule { }
